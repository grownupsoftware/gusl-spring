package gusl.application;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @author dhudson
 * @since 29/12/2020
 */
@Component
public class ApplicationUtils implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public String[] getBeanDefinitionNames() {
        return applicationContext.getBeanDefinitionNames();
    }

    public Class getBeanClass(String beanName) {
        return applicationContext.getBean(beanName).getClass();
    }

    public AnnotationConfigWebApplicationContext getContext() {
        return (AnnotationConfigWebApplicationContext) applicationContext;
    }

    public <T> T getBean(Class<T> requiredType) throws BeansException {
        return applicationContext.getBean(requiredType);
    }

    public String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType) throws BeansException {
        return applicationContext.getBeanNamesForAnnotation(annotationType);
    }

    public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
        return applicationContext.getBeansOfType(type);
    }

    public void autowire(Object existing) {
        applicationContext.getAutowireCapableBeanFactory().autowireBean(existing);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
