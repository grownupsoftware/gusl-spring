package gusl.application;

import lombok.CustomLog;
import org.glassfish.hk2.api.ActiveDescriptor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Finals with no getters means that we have to copy the class ..
 *
 * @author dhudson
 * @since 26/11/2021
 */
@CustomLog
public class GUSLAnnotationConfigServletWebServerApplicationContext extends ServletWebServerApplicationContext
        implements AnnotationConfigRegistry {

    private final AnnotatedBeanDefinitionReader reader;

    private final ClassPathBeanDefinitionScanner scanner;

    private final Set<Class<?>> annotatedClasses = new LinkedHashSet<>();

    private String[] basePackages;

    /**
     * Create a new {@link AnnotationConfigServletWebServerApplicationContext} that needs
     * to be populated through {@link #register} calls and then manually
     * {@linkplain #refresh refreshed}.
     */
    public GUSLAnnotationConfigServletWebServerApplicationContext() {
        this.reader = new AnnotatedBeanDefinitionReader(this);
        this.scanner = new ClassPathBeanDefinitionScanner(this);
        setup();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Delegates given environment to underlying {@link AnnotatedBeanDefinitionReader} and
     * {@link ClassPathBeanDefinitionScanner} members.
     */
    @Override
    public void setEnvironment(ConfigurableEnvironment environment) {
        super.setEnvironment(environment);
        this.reader.setEnvironment(environment);
        this.scanner.setEnvironment(environment);
    }

    /**
     * Provide a custom {@link BeanNameGenerator} for use with
     * {@link AnnotatedBeanDefinitionReader} and/or
     * {@link ClassPathBeanDefinitionScanner}, if any.
     * <p>
     * Default is
     * {@link AnnotationBeanNameGenerator}.
     * <p>
     * Any call to this method must occur prior to calls to {@link #register(Class...)}
     * and/or {@link #scan(String...)}.
     *
     * @param beanNameGenerator the bean name generator
     * @see AnnotatedBeanDefinitionReader#setBeanNameGenerator
     * @see ClassPathBeanDefinitionScanner#setBeanNameGenerator
     */
    public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
        this.reader.setBeanNameGenerator(beanNameGenerator);
        this.scanner.setBeanNameGenerator(beanNameGenerator);
        getBeanFactory().registerSingleton(AnnotationConfigUtils.CONFIGURATION_BEAN_NAME_GENERATOR, beanNameGenerator);
    }

    /**
     * Set the {@link ScopeMetadataResolver} to use for detected bean classes.
     * <p>
     * The default is an {@link AnnotationScopeMetadataResolver}.
     * <p>
     * Any call to this method must occur prior to calls to {@link #register(Class...)}
     * and/or {@link #scan(String...)}.
     *
     * @param scopeMetadataResolver the scope metadata resolver
     */
    public void setScopeMetadataResolver(ScopeMetadataResolver scopeMetadataResolver) {
        this.reader.setScopeMetadataResolver(scopeMetadataResolver);
        this.scanner.setScopeMetadataResolver(scopeMetadataResolver);
    }

    /**
     * Register one or more annotated classes to be processed. Note that
     * {@link #refresh()} must be called in order for the context to fully process the new
     * class.
     * <p>
     * Calls to {@code #register} are idempotent; adding the same annotated class more
     * than once has no additional effect.
     *
     * @param annotatedClasses one or more annotated classes, e.g. {@code @Configuration}
     *                         classes
     * @see #scan(String...)
     * @see #refresh()
     */
    @Override
    public final void register(Class<?>... annotatedClasses) {
        Assert.notEmpty(annotatedClasses, "At least one annotated class must be specified");
        this.annotatedClasses.addAll(Arrays.asList(annotatedClasses));
    }

    /**
     * Perform a scan within the specified base packages. Note that {@link #refresh()}
     * must be called in order for the context to fully process the new class.
     *
     * @param basePackages the packages to check for annotated classes
     * @see #register(Class...)
     * @see #refresh()
     */
    @Override
    public final void scan(String... basePackages) {
        Assert.notEmpty(basePackages, "At least one base package must be specified");
        this.basePackages = basePackages;
    }

    @Override
    protected void prepareRefresh() {
        this.scanner.clearCache();
        super.prepareRefresh();
    }

    @Override
    protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        super.postProcessBeanFactory(beanFactory);
        if (this.basePackages != null && this.basePackages.length > 0) {
            this.scanner.scan(this.basePackages);
        }
        if (!this.annotatedClasses.isEmpty()) {
            this.reader.register(ClassUtils.toClassArray(this.annotatedClasses));
        }
    }

    private void setup() {

        GUSLSpringServiceLocator.bootstrap();

        for (ActiveDescriptor<?> ad : GUSLSpringServiceLocator.getDescriptors()) {
            for (Type type : ad.getContractTypes()) {
                Class<?> classType = ((Class) type);
                if (classType.isInterface()) {
                    logger.info("Registering {}:{} for Autowire", ad.getImplementation(), classType);
                    reader.getRegistry().registerBeanDefinition(classType.getName(),
                            createBeanDefinition(classType, GUSLSpringServiceLocator.getService(classType)));
                }
            }
        }
    }

    private <T> BeanDefinition createBeanDefinition(Class<T> interfaceClass, Object implementation) {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(interfaceClass, () -> (T) implementation);
        builder.setScope(BeanDefinition.SCOPE_SINGLETON);
        builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
        builder.setSynthetic(true);
        return builder.getBeanDefinition();
    }
}
