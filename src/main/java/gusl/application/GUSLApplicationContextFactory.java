package gusl.application;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationContextFactory;
import org.springframework.boot.WebApplicationType;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author dhudson
 * @since 26/11/2021
 */
@Log4j2
public class GUSLApplicationContextFactory implements ApplicationContextFactory {

    @Override
    public ConfigurableApplicationContext create(WebApplicationType webApplicationType) {
//        log.info("WebApplicationType {}", webApplicationType);
        return new GUSLAnnotationConfigServletWebServerApplicationContext();
    }
}
