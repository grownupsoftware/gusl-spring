package gusl.application;

import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.glassfish.hk2.api.ActiveDescriptor;
import org.glassfish.hk2.api.ServiceHandle;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import javax.inject.Singleton;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * LmServiceLocator is a wrap around the Hk2 Service Locator.
 *
 * @author dhudson
 */
@CustomLog
public class GUSLSpringServiceLocator {

    private static ServiceLocator theServiceLocator = null;

    public static final String[] PACKAGE_PREFIXES = new String[]{"gusl"};

    /**
     * Likely to be called in non-servlet mode
     */
    public static synchronized void bootstrap() {
        logger.debug("-- bootstrap --");

        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Service.class));
        scanner.addIncludeFilter(new AnnotationTypeFilter(Singleton.class));

        theServiceLocator = ServiceLocatorUtilities.createAndPopulateServiceLocator();

        Set<BeanDefinition> scan = scanner.findCandidateComponents("gusl");
        Class<?>[] result = new Class<?>[scan.size()];

        int i = 0;
        for (BeanDefinition definition : scan) {
            try {
                result[i++] = Class.forName(definition.getBeanClassName(), false, scanner.getResourceLoader().getClassLoader());
            } catch (ClassNotFoundException ex) {
                logger.warn("Can't load class {}", definition.getBeanClassName(), ex);
            }
        }

        ServiceLocatorUtilities.addClasses(theServiceLocator, result);
    }

    public static <T> T getInstanceOf(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.createAndInitialize(clazz);
    }

    public static <T> T getService(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.getService(clazz);
    }

    public static ServiceLocator getServiceLocator() {
        return theServiceLocator;
    }

    public static void inject(Object injectMe) {
        theServiceLocator.inject(injectMe);
    }

    /**
     * Find all @Service annotations for a particular class
     *
     * @param <T>   type of class
     * @param clazz the class
     * @return list of service handlers that have the underlying service
     */
    public static <T> List<ServiceHandle<T>> getAllServiceHandlers(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.getAllServiceHandles(clazz);
    }

    public static void logServices() {
        if (theServiceLocator == null) {
            bootstrap();
        }

        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", "Scope", "Class", "Interface(s)"));
        builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", "-----", "-----", "------------"));

        Utils.safeStream(getDescriptors())
                .forEach(descriptor -> {
                    try {
                        builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", descriptor.getScope()
                                        .replace("javax.inject.", "")
                                        .replace("org.glassfish.hk2.api.", "")
                                        .replace("org.glassfish.jersey.process.internal.", ""),
                                descriptor.getImplementationClass().getCanonicalName(),
                                descriptor.getContractTypes()
                        ));
                    } catch (IllegalStateException ignore) {
                        //ignore 
                    }
                });

        logger.info("Services: [{}]\n{}", theServiceLocator.hashCode(), builder.toString());

    }

    public static List<ActiveDescriptor<?>> getDescriptors() {
        return theServiceLocator.getDescriptors(descriptor -> (hasCorrectPrefix(descriptor.getImplementation())));
    }

    public static boolean hasCorrectPrefix(String name) {
        for (String prefix : PACKAGE_PREFIXES) {
            if (name.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets Instances of a class that implements or extends the member class
     *
     * @param <T>         the type of instance
     * @param memberClazz the member class
     * @return the list of instances
     */
    public static <T> List<T> getInstancesWithSubTypeOf(Class<T> memberClazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        List<T> result = new ArrayList<>();
        Utils.safeStream(getDescriptors())
                .forEach(descriptor -> {
                    try {
                        Class<?> implementationClass = descriptor.getImplementationClass();
                        logger.debug("--> class: {} is member of {} : {}", implementationClass.getSimpleName(),
                                memberClazz.getSimpleName(), memberClazz.isAssignableFrom(implementationClass));

                        if (memberClazz.isAssignableFrom(implementationClass)) {
                            //T object = (T) theServiceLocator.createAndInitialize(implementationClass);
                            @SuppressWarnings("unchecked")
                            T object = (T) theServiceLocator.getService(implementationClass);
                            result.add(object);
                        }

                    } catch (IllegalStateException ex) {
                        // ignore 
                    }
                });
        return result;
    }

    public static List<Object> getInstanceWithClassAnnotation(Class<? extends Annotation> annotationType) {
        if (theServiceLocator == null) {
            bootstrap();
        }

        List<Object> result = new ArrayList<>();

        return result;
    }

    /**
     * Get all instances of a class (service) that contains methods that are
     * annotated with annotationType
     *
     * @param annotationType
     * @return List of instances that match the criteria
     */
    public static List<Object> getInstancesWithAnnotatedMethods(Class<? extends Annotation> annotationType) {
        if (theServiceLocator == null) {
            bootstrap();
        }

        List<Object> result = new ArrayList<>();
        Utils.safeStream(getDescriptors())
                .forEach(descriptor -> {
                    try {
                        Class<?> implementationClass = descriptor.getImplementationClass();
                        if (walkClassLookingForAnnotations(implementationClass, annotationType)) {
                            result.add(theServiceLocator.getService(implementationClass));
                        }
                    } catch (IllegalStateException ignore) {
                    }
                });
        return result;

    }

    private static boolean walkClassLookingForAnnotations(Class<?> implementationClass, Class<? extends Annotation> annotationType) {
        while (implementationClass != null) {
            for (Method m : implementationClass.getDeclaredMethods()) {
                if (m.isAnnotationPresent(annotationType)) {
                    return true;
                }
            }

            implementationClass = implementationClass.getSuperclass();
        }
        return false;
    }

    /**
     * Close the context so that a new one can be re-loaded.
     * <b>Only to be used in testing!</b>
     */
    public static void close() {
        if (theServiceLocator != null) {
            theServiceLocator.shutdown();
        }
    }

    public static void bindAbstractBinders() {
        if (theServiceLocator == null) {
            bootstrap();
        }
        List<AbstractBinder> binders = getInstancesWithSubTypeOf(AbstractBinder.class);
        logger.debug("found binders: {}", binders);
        ServiceLocatorUtilities.bind(theServiceLocator, binders.toArray(new AbstractBinder[binders.size()]));
    }

}
