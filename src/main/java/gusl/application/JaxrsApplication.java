package gusl.application;

import lombok.CustomLog;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;

/**
 * @author dhudson
 * @since 24/11/2021
 */
@Component
@ApplicationPath("/api")
@CustomLog
public class JaxrsApplication extends ResourceConfig {

    @Inject
    public JaxrsApplication(final ServletContext context, final BeanFactory beanFactory, ApplicationContext applicationContext) {
//        property(ServerProperties.WADL_FEATURE_DISABLE, true);
//        ServiceLocator locator = ServiceLocatorUtilities.createAndPopulateServiceLocator();
//        ServiceLocatorUtilities.addOneConstant(locator, beanFactory);
//        ServiceLocatorUtilities.enableImmediateScope(locator);
//
//        GUSLServiceLocator.bootstrap(locator);
//
//        GUSLServiceLocator.logServices();
//
//        SpringBridge.getSpringBridge().initializeSpringBridge(locator);
//
//        SpringIntoHK2Bridge springBridge = locator.getService(SpringIntoHK2Bridge.class);
//        springBridge.bridgeSpringBeanFactory(beanFactory);
//        log.info("I got here ....{} : {}", applicationContext, ElasticDelegate.class.getName());
//        applicationContext.getAutowireCapableBeanFactory().configureBean(GUSLServiceLocator.getService(ElasticDelegate.class),
//                ElasticDelegate.class.getName());


//        ElasticDelegate delegate = new ElasticDelegateImpl();
        //ElasticClient elasticClient = new ElasticClientImpl();
//        ElasticClientImpl impl = applicationContext.getAutowireCapableBeanFactory().createBean(ElasticClientImpl.class);
//        applicationContext.getAutowireCapableBeanFactory().configureBean(impl, ElasticClient.class.getName());
//        applicationContext.getAutowireCapableBeanFactory().initializeBean(elasticClient, ElasticClient.class.getName());

//        applicationContext.getAutowireCapableBeanFactory().autowireBean(delegate);
//        applicationContext.getAutowireCapableBeanFactory().initializeBean(delegate, ElasticDelegate.class.getName());
//        applicationContext.getAutowireCapableBeanFactory().initializeBean(elasticClient, ElasticClient.class.getName());

//        for (String name : applicationContext.getBeanDefinitionNames()) {
//            log.info("{}", name);
//        }

        logger.info("{}", applicationContext.getClass().getName());

    }
}
